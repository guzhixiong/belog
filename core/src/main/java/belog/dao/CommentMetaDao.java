package belog.dao;

import belog.dao.common.CommonDao;
import belog.pojo.po.CommentMeta;

/**
 * @author Beldon
 */
public interface CommentMetaDao extends CommonDao<CommentMeta> {
}
