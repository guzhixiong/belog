package belog.dao;


import belog.dao.common.CommonDao;
import belog.pojo.po.TermRelationships;

/**
 * @author Beldon 2015/11/7
 */
public interface TermRelationshipsDao extends CommonDao<TermRelationships> {
}
