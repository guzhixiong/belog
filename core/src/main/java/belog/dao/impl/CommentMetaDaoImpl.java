package belog.dao.impl;


import belog.dao.CommentMetaDao;
import belog.dao.common.impl.CommonDaoImpl;
import belog.pojo.po.CommentMeta;
import org.springframework.stereotype.Repository;

/**
 * @author Beldon
 */
@Repository("CommentMetaDao")
public class CommentMetaDaoImpl extends CommonDaoImpl<CommentMeta> implements CommentMetaDao {

}
