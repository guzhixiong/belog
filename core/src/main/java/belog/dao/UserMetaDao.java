package belog.dao;


import belog.dao.common.CommonDao;
import belog.pojo.po.UserMeta;

/**
 * @author Beldon
 */
public interface UserMetaDao extends CommonDao<UserMeta> {
}
