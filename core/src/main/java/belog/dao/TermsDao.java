package belog.dao;


import belog.dao.common.CommonDao;
import belog.pojo.po.Terms;

/**
 * @author Beldon
 */
public interface TermsDao extends CommonDao<Terms> {
}
